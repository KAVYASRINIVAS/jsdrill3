const reduce = require('../reduce');

function arrtoNum(arr,s){
    let num=[];
    for(let i=s;i<arr.length;i++){
        num=num+arr[i];
    }
    return num;
}

const items = [1, 2, 3, 4, 5, 5];
console.log(reduce(items,arrtoNum,1));
console.log(reduce(items,arrtoNum));
console.log(reduce(items,arrtoNum,10));
console.log(reduce([],arrtoNum,1));
console.log(reduce([],arrtoNum,1));