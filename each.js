function each(elements, cb) {
    if (!elements || !cb) {
        return [];
    }
    for (let i = 0; i < elements.length; i++) {
        cb(elements[i], i, elements);
    }
}

module.exports = each;

/*
const items = [1, 2, 3, 4, 5, 5];
each(items, function areaOfSquare(ele, i, elements) {
    let res = ele * ele;
    console.log(res);
    return res;
});
*/
